''' Serigraphy v1.0 - DXF file screen printing for test points
    Angel Emmanuel Gonzalez Guzman '''

import sys
import math
from dxfwrite import DXFEngine as dxf
from shapely.geometry import Polygon # Polygons Comparison

dxf_file = dxf.drawing('test.dxf')
coordinate_file = "Serigrafia BOT.txt"

#---->GLOBALS
TP_DIAMETER = 0.06 # Test Point Diameter
LABEL_HGT = 0.07 # Labels height
FINE_ADJ = 0.001 # Fine adjustment for labels
FINE_STEP_GRID = 0.001 # Fine adjustment step for grid
FINE_STEP_LABEL = 0.005 # Fine adjustment step for label

diff_x = LABEL_HGT - TP_DIAMETER #0.01 en 'X' if label size is 0.07"
diff_y = (LABEL_HGT - TP_DIAMETER)/2 #0.005 en 'Y' if label size is 0.07"
countTP = 0 # Test Point count
nTPOverlapped = 0 # Number of Test Points overlapped
nLabelsOverlapped = 0 # Number of labels overlapped

#----> LISTS
labels = [] # Label names
x_TP = [] # Coordinates X for Test Point
y_TP = [] # Coordinates Y for Test Point
# Vertices of rectangles in polygons
x_vertex1 = []
x_vertex2 = []
x_vertex3 = []
x_vertex4 = []
y_vertex1 = []
y_vertex2 = []
y_vertex3 = []
y_vertex4 = []
# Lists of dictionaries for overlapping test points and labels
TP_overlapping = []
labels_overlapping = []

#----> GET DRAWING PROPERTIES
def getDrawingProperties(file):
    # prop_file = open(file, 'r')
    with open(file, 'r') as prop_file:
        lines = prop_file.read().splitlines()
        for line in lines:
            data = line.split('\t')
            labels.append(data[0])
            x_TP.append(data[1])
            y_TP.append(data[2])

#----> DRAW TEST POINT
def drawTestPoint(name, diameter, x, y):
    countPolyg = 0
    diameter = float(diameter)
    x = float(x)
    y = float(y)
    radius = diameter/2
    circle = dxf.circle(radius, (x, y))
    circle['layer'] = 'TEST_POINTS'
    circle['color'] = 7
    circle['thickness'] = 2.0
    # Get Rectangle-Circle Vertex 1
    x1_RC = x - radius
    y1_RC = y - radius
    # Get Rect-Circ Vertex 2
    x2_RC = x1_RC + diameter
    y2_RC = y1_RC
    # Get Rect-Circ Vertex 3
    x3_RC = x2_RC
    y3_RC = y2_RC + diameter
    # Get Rect-Circ Vertex 4
    x4_RC = x3_RC - diameter
    y4_RC = y3_RC

    Pol1 = Polygon([(x1_RC, y1_RC), (x2_RC, y2_RC), (x3_RC, y3_RC), (x4_RC, y4_RC)])

    # print('Pol1 = ')
    # print(Pol1)
    nPolyg = len(x_vertex1)
    if (nPolyg == 0):
        x_vertex1.append(x1_RC)
        y_vertex1.append(y1_RC)
        x_vertex2.append(x2_RC)
        y_vertex2.append(y2_RC)
        x_vertex3.append(x3_RC)
        y_vertex3.append(y3_RC)
        x_vertex4.append(x4_RC)
        y_vertex4.append(y4_RC)
        dxf_file.add(circle)
        return False
    else:
        while (nPolyg > countPolyg):
            Pol2 = Polygon([(x_vertex1[countPolyg], y_vertex1[countPolyg]), (x_vertex2[countPolyg], y_vertex2[countPolyg]), (x_vertex3[countPolyg], y_vertex3[countPolyg]), (x_vertex4[countPolyg], y_vertex4[countPolyg])])
            # print('Pol2 = ')
            # print(Pol2)
            intersects = calcPolygonIntersection(Pol1, Pol2)
            # print(intersects)
            # if intersects is False:
                # print('\n')
            if (intersects is True):
                    # Overlapped Test Points Detected
                    TP_overlapped = {'name': name, 'x': x, 'y': y}
                    TP_overlapping.append(TP_overlapped)
                    return True
            elif (intersects is False and countPolyg == nPolyg - 1):
                x_vertex1.append(x1_RC)
                y_vertex1.append(y1_RC)
                x_vertex2.append(x2_RC)
                y_vertex2.append(y2_RC)
                x_vertex3.append(x3_RC)
                y_vertex3.append(y3_RC)
                x_vertex4.append(x4_RC)
                y_vertex4.append(y4_RC)
                dxf_file.add(circle)
                return False
            countPolyg += 1

#----> DRAW GRID
def drawGrid(x, y, dir, step, cntStep):
    intersectFound = False
    X_SuccessfulAdjusted = False
    noGridAdjusted = False

    countTP = 0
    countLine = 0

    colLines = []
    rowLines = []

    x1_col_grid = []
    y1_col_grid = []
    x2_col_grid = []
    y2_col_grid = []
    x1_row_grid = []
    y1_row_grid = []
    x2_row_grid = []
    y2_row_grid = []

    # Get number of columns and rows
    minCols = min(x_TP)
    maxCols = max(x_TP)
    minRows = max(y_TP)
    maxRows = min(y_TP)
    diffCols = maxCols - minCols
    diffRows = minRows - maxRows
    nCols = math.ceil(diffCols) # Round if a decimal is found
    nRows = math.ceil(diffRows) # Round if a decimal is found

    for col in range (0, nCols+1):
        x1_col_grid.append(x+col)
        y1_col_grid.append(y)
        x2_col_grid.append(x+col)
        y2_col_grid.append(y-nRows)
        colLine = Polygon([(x+col, y), (x+col, y-nRows), (x+col, y), (x+col, y-nRows)])
        colLines.append(colLine)
        #traceFigure(x+col, y, x+col, y-nRows, x+col, y, x+col, y-nRows)
    for row in range(0, nRows+1):
        x1_row_grid.append(x)
        y1_row_grid.append(y-row)
        x2_row_grid.append(x+nCols)
        y2_row_grid.append(y-row)
        rowLine = Polygon([(x, y-row), (x+nCols, y-row), (x, y-row), (x+nCols, y-row)])
        rowLines.append(rowLine)
        #traceFigure(x, y-row, x+nCols, y-row, x, y-row, x+nCols, y-row)

    nTP = len(x_vertex1)
    nColLines = len(colLines)
    nRowLines = len(rowLines)

    # Grid Movement in X
    while(nTP > countTP):
        Pol1 = Polygon([(x_vertex1[countTP], y_vertex1[countTP]), (x_vertex2[countTP], y_vertex2[countTP]), (x_vertex3[countTP], y_vertex3[countTP]), (x_vertex4[countTP], y_vertex4[countTP])])
        while (nColLines > countLine):
            Pol2 = colLines[countLine]
            intersects = calcPolygonIntersection(Pol1, Pol2)

            if (intersects is True):
                # Grid movement
                if (dir == 'left'): # Adjust to left
                    x = x - step
                    cntStep -= 1
                    if (cntStep == -250):
                        dir = 'right'
                        x = x + (step * 250)
                        cntStep = 0
                    intersectFound = True
                    break
                elif (dir == 'right'):  # Adjust to right
                    x = x + step
                    cntStep += 1
                    if (cntStep == 250):
                        dir = 'origin'
                        x = x - (step * 250)
                        cntStep = 0
                    intersectFound = True
                    break
                elif (dir == 'origin'):  # Grid could not be adjusted
                    print("The grid could not be adjusted in X, it will print with errors!")
                    # Print row numbers
                    x_origin_nums = x - 0.6
                    y_origin_nums = y - 0.65
                    cntNums = nRows
                    for n in range(nRows, 0, -1):
                        number = len(str(n))
                        if (number == 2):
                            num = dxf.text(str(cntNums), (x_origin_nums, y_origin_nums), height=0.35, rotation=0)
                            num['layer'] = 'TRACE'
                            num['color'] = 7
                            dxf_file.add(num)
                            y_origin_nums -= 1
                            cntNums -= 1
                        elif (number == 1):
                            x_origin_nums = x - 0.45
                            num = dxf.text(str(cntNums), (x_origin_nums, y_origin_nums), height=0.35, rotation=0)
                            num['layer'] = 'TRACE'
                            num['color'] = 7
                            dxf_file.add(num)
                            y_origin_nums -= 1
                            cntNums -= 1
                    # Print column letters
                    x_origin_letters = x + 0.35
                    y_origin_letters = y + 0.2
                    for c in range(65, 65 + nCols):  # A-Z in ASCII
                        letter = dxf.text(chr(c), (x_origin_letters, y_origin_letters), height=0.35, rotation=0)
                        letter['layer'] = 'TRACE'
                        letter['color'] = 7
                        dxf_file.add(letter)
                        x_origin_letters += 1
                    # Print Grid
                    for i in range(0, len(x1_col_grid)):
                        traceFigure(x1_col_grid[i], y1_col_grid[i], x2_col_grid[i], y2_col_grid[i], x1_col_grid[i], y1_col_grid[i], x2_col_grid[i], y2_col_grid[i], 7)
                        x_vertex1.append(x1_col_grid[i])
                        y_vertex1.append(y1_col_grid[i])
                        x_vertex2.append(x2_col_grid[i])
                        y_vertex2.append(y2_col_grid[i])
                        x_vertex3.append(x1_col_grid[i])
                        y_vertex3.append(y1_col_grid[i])
                        x_vertex4.append(x2_col_grid[i])
                        y_vertex4.append(y2_col_grid[i])
                    for i in range(0, len(x1_row_grid)):
                        traceFigure(x1_row_grid[i], y1_row_grid[i], x2_row_grid[i], y2_row_grid[i], x1_row_grid[i], y1_row_grid[i], x2_row_grid[i], y2_row_grid[i], 7)
                        x_vertex1.append(x1_row_grid[i])
                        y_vertex1.append(y1_row_grid[i])
                        x_vertex2.append(x2_row_grid[i])
                        y_vertex2.append(y2_row_grid[i])
                        x_vertex3.append(x1_row_grid[i])
                        y_vertex3.append(y1_row_grid[i])
                        x_vertex4.append(x2_row_grid[i])
                        y_vertex4.append(y2_row_grid[i])
                    noGridAdjusted = True
                    break

            elif (intersects is False and countTP == nTP - 1 and countLine == nColLines - 1):
                X_SuccessfulAdjusted = True
                dir = 'up'
                intersectFound = False
            countLine += 1
        if (intersectFound is True or noGridAdjusted is True): break
        countLine = 0
        countTP += 1

    countTP = 0
    countLine = 0

    # Grid Movement in Y
    if (X_SuccessfulAdjusted):
        while (nTP > countTP):
            Pol1 = Polygon([(x_vertex1[countTP], y_vertex1[countTP]), (x_vertex2[countTP], y_vertex2[countTP]),
                            (x_vertex3[countTP], y_vertex3[countTP]), (x_vertex4[countTP], y_vertex4[countTP])])
            while (nRowLines > countLine):
                Pol2 = rowLines[countLine]
                intersects = calcPolygonIntersection(Pol1, Pol2)

                if (intersects is True):
                    # Grid movement
                    if (dir == 'up'):  # Adjust to up
                        y = y + step
                        cntStep += 1
                        if (cntStep == 250):
                            dir = 'down'
                            x = x - (step * 250)
                            cntStep = 0
                        intersectFound = True
                        break
                    elif (dir == 'down'):  # Adjust to down
                        y = y - step
                        cntStep -= 1
                        if (cntStep == -250):
                            dir = 'origin'
                            y = y + (step * 250)
                            cntStep = 0
                        intersectFound = True
                        break
                    elif (dir == 'origin'):  # Grid could not be adjusted
                        print("The grid could not be adjusted in Y, it will print with errors!")
                        # Print row numbers
                        x_origin_nums = x - 0.6
                        y_origin_nums = y - 0.65
                        cntNums = nRows
                        for n in range(nRows, 0, -1):
                            number = len(str(n))
                            if (number == 2):
                                num = dxf.text(str(cntNums), (x_origin_nums, y_origin_nums), height=0.35, rotation=0)
                                num['layer'] = 'TRACE'
                                num['color'] = 7
                                dxf_file.add(num)
                                y_origin_nums -= 1
                                cntNums -= 1
                            elif (number == 1):
                                x_origin_nums = x - 0.45
                                num = dxf.text(str(cntNums), (x_origin_nums, y_origin_nums), height=0.35, rotation=0)
                                num['layer'] = 'TRACE'
                                num['color'] = 7
                                dxf_file.add(num)
                                y_origin_nums -= 1
                                cntNums -= 1
                        # Print column letters
                        x_origin_letters = x + 0.35
                        y_origin_letters = y + 0.2
                        for c in range(65, 65 + nCols):  # A-Z in ASCII
                            letter = dxf.text(chr(c), (x_origin_letters, y_origin_letters), height=0.35, rotation=0)
                            letter['layer'] = 'TRACE'
                            letter['color'] = 7
                            dxf_file.add(letter)
                            x_origin_letters += 1
                        # Print Grid
                        for i in range(0, len(x1_col_grid)):
                            traceFigure(x1_col_grid[i], y1_col_grid[i], x2_col_grid[i], y2_col_grid[i], x1_col_grid[i], y1_col_grid[i], x2_col_grid[i], y2_col_grid[i], 7)
                            x_vertex1.append(x1_col_grid[i])
                            y_vertex1.append(y1_col_grid[i])
                            x_vertex2.append(x2_col_grid[i])
                            y_vertex2.append(y2_col_grid[i])
                            x_vertex3.append(x1_col_grid[i])
                            y_vertex3.append(y1_col_grid[i])
                            x_vertex4.append(x2_col_grid[i])
                            y_vertex4.append(y2_col_grid[i])
                        for i in range(0, len(x1_row_grid)):
                            traceFigure(x1_row_grid[i], y1_row_grid[i], x2_row_grid[i], y2_row_grid[i], x1_row_grid[i], y1_row_grid[i], x2_row_grid[i], y2_row_grid[i], 7)
                            x_vertex1.append(x1_row_grid[i])
                            y_vertex1.append(y1_row_grid[i])
                            x_vertex2.append(x2_row_grid[i])
                            y_vertex2.append(y2_row_grid[i])
                            x_vertex3.append(x1_row_grid[i])
                            y_vertex3.append(y1_row_grid[i])
                            x_vertex4.append(x2_row_grid[i])
                            y_vertex4.append(y2_row_grid[i])
                        noGridAdjust = True
                        break

                elif (intersects is False and countTP == nTP - 1 and countLine == nRowLines - 1):
                    # Print row numbers
                    x_origin_nums = x - 0.6
                    y_origin_nums = y - 0.65
                    cntNums = nRows
                    for n in range(nRows, 0, -1):
                        number = len(str(n))
                        if (number == 2):
                            num = dxf.text(str(cntNums), (x_origin_nums, y_origin_nums), height=0.35, rotation=0)
                            num['layer'] = 'TRACE'
                            num['color'] = 7
                            dxf_file.add(num)
                            y_origin_nums -= 1
                            cntNums -= 1
                        elif (number == 1):
                            x_origin_nums = x - 0.45
                            num = dxf.text(str(cntNums), (x_origin_nums, y_origin_nums), height=0.35, rotation=0)
                            num['layer'] = 'TRACE'
                            num['color'] = 7
                            dxf_file.add(num)
                            y_origin_nums -= 1
                            cntNums -= 1
                    # Print column letters
                    x_origin_letters = x + 0.35
                    y_origin_letters = y + 0.2
                    for c in range(65, 65+nCols): # A-Z in ASCII
                        letter = dxf.text(chr(c), (x_origin_letters, y_origin_letters), height=0.35, rotation=0)
                        letter['layer'] = 'TRACE'
                        letter['color'] = 7
                        dxf_file.add(letter)
                        x_origin_letters += 1
                    # Print Grid
                    for i in range(0, len(x1_col_grid)):
                        traceFigure(x1_col_grid[i], y1_col_grid[i], x2_col_grid[i], y2_col_grid[i], x1_col_grid[i], y1_col_grid[i], x2_col_grid[i], y2_col_grid[i], 7)
                        x_vertex1.append(x1_col_grid[i])
                        y_vertex1.append(y1_col_grid[i])
                        x_vertex2.append(x2_col_grid[i])
                        y_vertex2.append(y2_col_grid[i])
                        x_vertex3.append(x1_col_grid[i])
                        y_vertex3.append(y1_col_grid[i])
                        x_vertex4.append(x2_col_grid[i])
                        y_vertex4.append(y2_col_grid[i])
                    for i in range(0, len(x1_row_grid)):
                        traceFigure(x1_row_grid[i], y1_row_grid[i], x2_row_grid[i], y2_row_grid[i], x1_row_grid[i], y1_row_grid[i], x2_row_grid[i], y2_row_grid[i], 7)
                        x_vertex1.append(x1_row_grid[i])
                        y_vertex1.append(y1_row_grid[i])
                        x_vertex2.append(x2_row_grid[i])
                        y_vertex2.append(y2_row_grid[i])
                        x_vertex3.append(x1_row_grid[i])
                        y_vertex3.append(y1_row_grid[i])
                        x_vertex4.append(x2_row_grid[i])
                        y_vertex4.append(y2_row_grid[i])
                    intersectFound = False
                countLine += 1
            if (intersectFound is True or noGridAdjusted is True): break
            countLine = 0
            countTP += 1

    if (intersectFound):
        # The function returns to itself with new values
        drawGrid(x, y, dir, step, cntStep)
    else:
        # Function ends
        return

#----> DRAW LABELS
def drawLabel(name, x, y, hgt, rot, dir, step, cntStep):
    intersectFound = False
    countPolyg = 0
    width = calcLabelSize(hgt, name)
    # Getting label vertices
    if(rot == 0):
        # Get Rectangle-Label Vertex 1
        x1_RL = x
        y1_RL = y
        # Get Rectangle-Label Vertex 2
        x2_RL = x1_RL + width
        y2_RL = y1_RL
        # Get Rectangle-Label Vertex 3
        x3_RL = x2_RL
        y3_RL = y2_RL + hgt
        #Get Rectangle-Label Vertex 4
        x4_RL = x3_RL - width
        y4_RL = y3_RL
    elif(rot == 90):
        # Get Rectangle-Label Vertex 1
        x1_RL = x
        y1_RL = y
        #Get Rectangle-Label Vertex 2
        x2_RL = x1_RL
        y2_RL = y1_RL + width
        # Get Rectangle-Label Vertex 3
        x3_RL = x2_RL - hgt
        y3_RL = y2_RL
        # Get Rectangle-Label Vertex 4
        x4_RL = x3_RL
        y4_RL = y3_RL - width
    elif(rot == 270):
        # Get Rectangle-Label Vertex 1
        x1_RL = x
        y1_RL = y
        # Get Rectangle-Label Vertex 2
        x2_RL = x1_RL
        y2_RL = y1_RL - width
        # Get Rectangle-Label Vertex 3
        x3_RL = x2_RL + hgt
        y3_RL = y2_RL
        # Get Rectangle-Label Vertex 4
        x4_RL = x3_RL
        y4_RL = y3_RL + width
    if (rot == 360):
        # Get Rectangle-Label Vertex 1
        x1_RL = x
        y1_RL = y
        # Get Rectangle-Label Vertex 2
        x2_RL = x1_RL + width
        y2_RL = y1_RL
        # Get Rectangle-Label Vertex 3
        x3_RL = x2_RL
        y3_RL = y2_RL + hgt
        # Get Rectangle-Label Vertex 4
        x4_RL = x3_RL - width
        y4_RL = y3_RL

    Pol1 = Polygon([(x1_RL, y1_RL), (x2_RL, y2_RL), (x3_RL, y3_RL), (x4_RL, y4_RL)])
    # traceFigure(x1_RL, y1_RL, x2_RL, y2_RL, x3_RL, y3_RL, x4_RL, y4_RL, 3)
    # print('Pol1 = ')
    # print(Pol1)
    nPolyg = len(x_vertex1)
    while(nPolyg > countPolyg):
        Pol2 = Polygon([(x_vertex1[countPolyg], y_vertex1[countPolyg]), (x_vertex2[countPolyg], y_vertex2[countPolyg]), (x_vertex3[countPolyg], y_vertex3[countPolyg]), (x_vertex4[countPolyg], y_vertex4[countPolyg])])
        # traceFigure(x_vertex1[countPolyg], y_vertex1[countPolyg], x_vertex2[countPolyg], y_vertex2[countPolyg], x_vertex3[countPolyg], y_vertex3[countPolyg], x_vertex4[countPolyg], y_vertex4[countPolyg], 3)
        # print('Pol2 = ')
        # print(Pol2)

        intersects = calcPolygonIntersection(Pol1, Pol2)
        # print(intersects)
        # if intersects is False:
        #    print('\n')
        if (intersects is True):
            # 90 Degree Rotation
            if (rot == 0):
                # Fine adjustment towards one direction or another
                if (dir == 'right'): # Adjust to up
                    y = y + step
                    cntStep += 1
                elif (dir == 'left'): # Adjust to down
                    y = y - step
                    cntStep -= 1
                elif ('rotate'): # 90 degree rotation if fine adjust is not possible
                    rot = 90
                    x = x - FINE_ADJ
                    y = y + hgt + diff_y
                    dir = 'right'
                # Step count
                if (cntStep == 10):
                    dir = 'left'
                    y = y - (step * 10)
                    cntStep = 0
                elif (cntStep == -10):
                    dir = 'rotate'
                    y = y + (step * 10)
                    cntStep = 0
                # LAST CODE
                #x = x - FINE_ADJ
                #y = y + hgt + diff_y
                #rot = 90
                intersectFound = True
                break
            # 270 Degree Rotation
            elif (rot == 90):
                # Fine adjustment towards one direction or another
                if (dir == 'right'): # Adjust to right
                    x = x + step
                    cntStep += 1
                elif (dir == 'left'): # Adjust to left
                    x = x - step
                    cntStep -= 1
                elif ('rotate'): # 270 degree rotation if fine adjust is not possible
                    rot = 270
                    x = x - TP_DIAMETER - diff_x
                    y = y - hgt - (diff_y*2)
                    dir = 'right'
                # Step count
                if (cntStep == 10):
                    dir = 'left'
                    x = x - (step * 10)
                    cntStep = 0
                elif (cntStep == -10):
                    dir = 'rotate'
                    x = x + (step * 10)
                    cntStep = 0
                # LAST CODE
                #x = x - TP_DIAMETER - diff_x
                #y = y - hgt - (diff_y*2)
                #rot = 270
                intersectFound = True
                break
            # 360 Degree Rotation
            elif (rot == 270):
                # Fine adjustment towards one direction or another
                if (dir == 'right'): # Adjust to right
                    x = x + step
                    cntStep += 1
                elif (dir == 'left'): # Adjust to left
                    x = x - step
                    cntStep -= 1
                elif ('rotate'): # 360 degree rotation if fine adjust is not possible
                    rot = 360
                    x = x + TP_DIAMETER + diff_x
                    y = y
                    dir = 'right'
                # Step count
                if (cntStep == 10):
                    dir = 'left'
                    x = x - (step * 10)
                    cntStep = 0
                elif (cntStep == -10):
                    dir = 'rotate'
                    x = x + (step * 10)
                    cntStep = 0
                # LAST CODE
                #x = x + TP_DIAMETER + diff_x
                #y = y
                #rot = 360
                intersectFound = True
                break
            # Does not fit
            elif (rot == 360):
                #  Overlapped label detected
                label_overlapped = {'name': name, 'x': x, 'y': y}
                labels_overlapping.append(label_overlapped)
                # Print the label overlapped
                x_vertex1.append(x1_RL)
                y_vertex1.append(y1_RL)
                x_vertex2.append(x2_RL)
                y_vertex2.append(y2_RL)
                x_vertex3.append(x3_RL)
                y_vertex3.append(y3_RL)
                x_vertex4.append(x4_RL)
                y_vertex4.append(y4_RL)
                label = dxf.text(name, (x, y), height=hgt, rotation=rot)
                label['layer'] = 'LABELS'
                label['color'] = 8
                dxf_file.add(label)
                intersectFound = False
                labelNotFit()
                break

        elif (intersects is False and countPolyg == nPolyg - 1):
            # Print the label
            x_vertex1.append(x1_RL)
            y_vertex1.append(y1_RL)
            x_vertex2.append(x2_RL)
            y_vertex2.append(y2_RL)
            x_vertex3.append(x3_RL)
            y_vertex3.append(y3_RL)
            x_vertex4.append(x4_RL)
            y_vertex4.append(y4_RL)
            label = dxf.text(name, (x, y), height=hgt, rotation=rot)
            label['layer'] = 'LABELS'
            label['color'] = 8
            dxf_file.add(label)
            intersectFound = False
            #traceFigure(x1_RL, y1_RL, x2_RL, y2_RL, x3_RL, y3_RL, x4_RL, y4_RL, 3)
        countPolyg += 1

    if (intersectFound):
        # The function returns to itself with new values
        drawLabel(name, x, y, hgt, rot, dir, step, cntStep)
    else:
        # Function ends
        return

#----> CALCULATE RECTANGLES OVER LABELS
def calcLabelSize(height, str):
    nChars = len(str)
    if(nChars == 1):
        width = (height / 2) + 0.015
        return width
    elif(nChars == 2):
        width = height + 0.035
        return width
    elif(nChars == 3):
        width = (height * 2) + 0.02
        return width
    elif(nChars == 4):
        width = (height * 3)
        return width
    else:
        print('The max number of characters allowed is 4!')
        sys.exit(0)

#----> CALCULATE POLYGON INTERSECTIONS
def calcPolygonIntersection(polygon1, polygon2):
    return polygon1.intersects(polygon2) #return true if polygons intersects

#----> FOUND LABELS THAT DOES NOT FIT OR OVERLAPPED
def labelNotFit():
    global nLabelsOverlapped
    nLabelsOverlapped += 1
    return

#----> TRACE A FIGURE IN DXF FILE
def traceFigure(x1, y1, x2, y2, x3, y3, x4, y4, color):
    trace = dxf.trace([(x1, y1), (x2, y2), (x3, y3), (x4, y4)], layer='0')
    trace['layer'] = 'TRACE'
    trace['color'] = color  # verde
    trace['thickness'] = 0.000001
    return dxf_file.add(trace)

#----> MAIN
if __name__=='__main__':
    #Get properties from txt file
    getDrawingProperties(coordinate_file)

    if(len(x_TP) == len(y_TP)):
        # Drawing test points
        while (len(x_TP) > countTP and len(y_TP) > countTP):
            foundOverlapped = drawTestPoint(labels[countTP], TP_DIAMETER, x_TP[countTP], y_TP[countTP])
            if (foundOverlapped) : nTPOverlapped += 1
            else: pass
            countTP += 1
        countTP = 0

        if (nTPOverlapped > 0):
            print('Overlapping test points were detected!')
            for TP in TP_overlapping:
                print(TP)
            print('DXF file not created! Please verify your coordinate file.')
            sys.exit(0)
        else: pass

        # Drawing grid
        x_TP = [float(i) for i in x_TP]
        y_TP = [float(i) for i in y_TP]
        x_grid_origin = float(min(x_TP)) - TP_DIAMETER
        y_grid_origin = float(max(y_TP)) + TP_DIAMETER
        drawGrid(x_grid_origin, y_grid_origin, 'left', FINE_STEP_GRID, 0)

        # Drawing labels
        while (len(x_TP) > countTP and len(y_TP) > countTP):
            x_circ = float(x_TP[countTP])
            y_circ = float(y_TP[countTP])
            radius = TP_DIAMETER/2
            x_label = x_circ + radius + diff_x
            y_label = y_circ - radius - diff_y
            drawLabel(labels[countTP], x_label, y_label, LABEL_HGT, 0, 'right', FINE_STEP_LABEL, 0)
            countTP += 1
        countTP = 0

        if (nLabelsOverlapped > 0):
            print('The following labels did not fit and were written overlapping:')
            for label in labels_overlapping:
                print(label)

    print('DXF file created successfully!')
    dxf_file.save()
